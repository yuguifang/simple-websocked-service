
//websocketd -port=8080 node.exe count.js

// 等待输入
process.stdin.resume()
process.stdin.setEncoding('utf8')

// 接收到消息的回调处理函数
process.stdin.on( 'data', function( data ) {
  // 向客户端返回消息
  process.stdout.write('你好,' + data.trim() +'\n' )
})

// 推送数据
setInterval(() => {
  let _random = Math.floor((Math.random()*100)+1);
  process.stdout.write( JSON.stringify(`${_random}`) +'\n' )
}, 1000);
