# 简易websocketd服务 

#### 介绍
使用websocketd搭建使用脚本的简易的websocket服务

#### 使用说明

1.  服务端：

1.1. 下载websocketd压缩包并解压到目录，将此目录添加到本机器的环境变量中；

网址： https://github.com/joewalnes/websocketd/releases

1.2. 进入websocketd目录，cmd中执行命令：

websocketd -port=8080 node.exe count.js

（事先安装node且路径添加到环境变量中）

2.web端

打开html页面点击连接。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0220/142206_38e8c5bb_7757390.png "屏幕截图.png")


参考网址：

https://github.com/joewalnes/websocketd/wiki/Ten-second-tutorial；

http://www.ruanyifeng.com/blog/2017/05/websocket.html；